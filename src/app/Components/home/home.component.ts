import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    this.owl();
    this.owl1();
    this.owl3();
  }

  owl() {
    $('#mobile_sliders').owlCarousel({
      // loop:true,
      margin: 5,
      // items: 2,
      nav: true,
      dots: false,
      responsive: {
        0: {
          items: 1,
          slideBy: 1
        },
        400: {
          items: 1,
          slideBy: 1
        },
        740: {
          items: 1,
          slideBy: 1
        },
        940: {
          items: 1,
          slideBy: 1
        }
      },
    })
  }
  owl1() {
    $('#mobile_sliders3').owlCarousel({
      // loop:true,
      margin: 10,
      // items: 2,
      nav: true,
      dots: false,
      responsive: {
        0: {
          items: 1,
          slideBy: 1
        },
        400: {
          items: 1,
          slideBy: 1
        },
        740: {
          items: 2,
          slideBy: 2
        },
        940: {
          items: 4,
          slideBy: 4
        }
      },
    })
  }

  owl3() {
    $('#mobile_sliders2').owlCarousel({
      // loop:true,
      margin: 5,
      // items: 2,
      nav: true,
      dots: false,
      responsive: {
        0: {
          items: 1,
          slideBy: 1
        },
        400: {
          items: 1,
          slideBy: 1
        },
        740: {
          items: 2,
          slideBy: 2
        },
        940: {
          items: 3,
          slideBy: 3
        }
      },
    })
  }
}
